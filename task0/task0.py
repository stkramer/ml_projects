import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn import linear_model


if __name__ == '__main__':
  
    train_set = pd.read_csv("train.csv")
    print(train_set.shape)

    
    train_y = train_set.iloc[1:10000, 1].to_numpy()
    train_x = train_set.iloc[1:10000, 2:12].to_numpy()
    model = linear_model.LinearRegression()
    model.fit(train_x, train_y)

    test_dataset = pd.read_csv("test.csv")
    x = test_dataset.iloc[:, 1:11].to_numpy()
    pred_y = model.predict(x)

    
    prediction = pd.DataFrame(test_dataset.iloc[:, 0])
    prediction['y'] = pred_y
    prediction.to_csv("prediction.csv", index=False)

    
    
    
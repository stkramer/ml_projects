from json.tool import main
from operator import index
import numpy as np
import pandas as pd
from sklearn import kernel_ridge
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error


if __name__ == '__main__':
    
    data = pd.read_csv('train.csv')
    paras = [0.1,1,10,100,200]
    cross_validation_errors=[]

    for l in paras:
        validation_error = 0
        for k in range(10):
            start_row = k*15
            end_row = (k+1) * 15

            validation_set = data.iloc[start_row:end_row, 0:14]

            train_set_1 = data.iloc[0:start_row, 0:14]
            train_set_2 = data.iloc[end_row:150, 0:14]
            frames = [train_set_1,train_set_2]
            train_set = pd.concat(frames)

            train_y = train_set.iloc[:, [0]].to_numpy()
            train_x = train_set.iloc[:, 1:14].to_numpy()


            model = kernel_ridge.KernelRidge(alpha=l, kernel="linear", degree=1)
            model.fit(train_x, train_y)

            validation_y = validation_set.iloc[:, [0]].to_numpy()
            validation_x = validation_set.iloc[:, 1:14].to_numpy()

            prediction = model.predict(validation_x)
            error = mean_squared_error(validation_y, prediction)**0.5
            validation_error += error
        
        cross_validation_errors.append((validation_error/10))


    solution = pd.DataFrame(cross_validation_errors)
    print(solution)
    solution.to_csv('errors.csv', index=False)




from sklearn import kernel_ridge
from sklearn import svm
from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np


if __name__ == '__main__':
    # Load training data
    train_dataset = pd.read_csv("train.csv")
    y = train_dataset.iloc[:, 0].to_numpy()
    x = train_dataset.iloc[:, 1:15].to_numpy()
    lambdas = [.1, 1, 10, 100, 200]
    err = []
    for l in lambdas:
        print("new lambda: " + str(l));
        curr_err = 0
        model = kernel_ridge.KernelRidge(alpha=l, kernel="linear", degree=1)
        for i in range(10):

            train_x = np.concatenate((x[:i*15], x[i*15+15:]))
            train_y = np.concatenate((y[:i*15], y[i*15+15:]))
            test_x = x[i*15:i*15+15]
            test_y = y[i*15:i*15+15]

            model.fit(train_x, train_y)

            predict_y = model.predict(test_x)
            k = mean_squared_error(test_y, predict_y)**0.5
            curr_err += k
            print(k)
        err.append(curr_err/10)
    print(err)
    prediction = pd.DataFrame(err)
    prediction.to_csv("prediction.csv", index=False)

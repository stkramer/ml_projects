import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression   
from sklearn.linear_model import Ridge
from sklearn import linear_model


def makesquares(x):
    return x*x

if __name__ == '__main__':
    
    train_set = pd.read_csv('train.csv')
    #print(train_set.shape)
    train_x = train_set.iloc[:, 2:7].to_numpy()
    train_y = train_set.iloc[:, [1]].to_numpy()

    linear_features = train_x
    #square_features = np.multiply(train_x, train_x)
    square_features = train_x * train_x
    exp_features = np.exp(train_x)
    cos_features = np.cos(train_x)


    feature_set = np.hstack((linear_features, square_features, exp_features, cos_features))
    feature_set = np.c_[feature_set, np.ones(train_set.shape[0])]

    #model = Ridge(alpha=0.001)
    model = linear_model.HuberRegressor()
    model.fit(feature_set,train_y)

    print(model.coef_)
    coeffs = model.coef_.reshape(-1,1)
    print(coeffs.shape)

    solution = pd.DataFrame(coeffs)
    solution.to_csv('coefficents.csv', index=False)
    


    """
    print(linear_features)
    print(square_features)
    print(feature_set.shape)
    print(feature_set)
    """

from operator import length_hint
import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer
from score_submission import get_score
from sklearn import svm
from sklearn import metrics
from joblib import dump, load
from sklearn.linear_model import Ridge, SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.kernel_ridge import KernelRidge
import math
from trainer import Trainer
from sklearn import ensemble
from imblearn.under_sampling import RandomUnderSampler as UnderSampler
from imblearn.over_sampling import RandomOverSampler as OverSampler
from sklearn import kernel_ridge



columns = ['pid', 'LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
           'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2', 'LABEL_Bilirubin_direct', 'LABEL_EtCO2', 'LABEL_Sepsis', 'LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']



def prepare_data_mean(train_features, filename):
    nan_value = train_features.iloc[0,3]
    print(nan_value)
    mean = train_features.mean()
    train_data = []

    for i in range (int (train_features.shape[0]/12)):
        patient = train_features.iloc[12*i : 12*(i+1), :]
        patient_mean = (patient.mean(axis=0))

        for j in range (train_features.shape[1]):
            #print(patient_mean.iloc[j])
            if (math.isnan(patient_mean.iloc[j])):
                patient_mean.iloc[j] = mean[j]
            
    
        train_data.append(patient_mean)

    
    #print(train_data)
    pd.DataFrame(train_data).to_csv(filename, index=False)


def prepare_data_zero(train_features, filename):
    nan_value = train_features.iloc[0,3]
    train_data = []
    for i in range (int (train_features.shape[0]/12)):

        patient = train_features.iloc[12*i : 12*(i+1), :]
        patient_mean = (patient.mean(axis=0))
        patient_mean = patient_mean.replace([nan_value], 0)
    
        train_data.append(patient_mean)
    
    pd.DataFrame(train_data).to_csv(filename, index=False)



def prepare_data_full_mean(train_features, filename):
    print(train_features.shape)
    print(train_features.isnull().sum())

    imp_mean = SimpleImputer(missing_values=np.nan,strategy='mean')
    imp_mean.fit(train_features)
    train_features = pd.DataFrame(imp_mean.transform(train_features))
    train_data = []
    print((train_features.shape[0])/12)
    
    for i in range (int(train_features.shape[0]/12)):
        patient = train_features.iloc[12*i : 12*(i+1), :]
        patient_mean = (patient.mean(axis=0)).to_numpy()
        train_data.append(patient_mean)
    
    
    pd.DataFrame(train_data).to_csv(filename, index=False)


if __name__ == '__main__':
    
    
    train_features = pd.read_csv("train_features.csv")
    train_labels = pd.read_csv("train_labels.csv")

    test_features = pd.read_csv("test_features.csv")

    #prepare_data_mean(train_features, 'train_data_mean.csv')
    #prepare_data_mean(test_features, 'test_data_mean.csv')


    #prepare_data_full_mean(test_features, 'test_data.csv')

    #prepare_data_full_mean(train_features, 'train_data.csv')

    #prepare_data_zero(test_features, 'test_data_zero.csv')
    #prepare_data_zero(train_features, 'train_data_zero.csv')

    train_data = pd.read_csv("train_data_zero.csv")
    test_data = pd.read_csv("test_data_zero.csv")
    pred = pd.DataFrame(columns=columns)
    pred['pid'] = test_data.iloc[:,0]

    print(pred)
    print(pred.shape[0])
    

    train_x = train_data.iloc[:, 1:].to_numpy()
    test_x = test_data.iloc[:, 1:].to_numpy()
    
    
    #task1

    for i in range(10):
        train_y = train_labels.iloc[:, [i+1]].to_numpy()
        #downsampling
        # us = UnderSampler()
        # train_x_sampled, train_y_sampled = us.fit_resample(train_x, np.ravel(train_y))

        # model = make_pipeline(StandardScaler(), ensemble.GradientBoostingClassifier(n_estimators=100))
        # model.fit(train_x_sampled, np.ravel(train_y_sampled))
        # dump(model, 'model_GBC_' + str(i+1) + '.joblib')
        model = load('model_GBC_' + str(i+1) + '.joblib')
        pred[columns[i+1]] = model.predict(test_x)
        print(i)
    
    
    #pred.to_csv("prediction.csv")

    #task 2
    train_y = train_labels.iloc[:, [11]].to_numpy()
    #downsampling
    # us = UnderSampler()
    # train_x_sampled, train_y_sampled = us.fit_resample(train_x, np.ravel(train_y))

    # model = make_pipeline(StandardScaler(),ensemble.GradientBoostingClassifier(n_estimators=200))
    # model.fit(train_x_sampled, np.ravel(train_y_sampled))
    # dump(model, 'model_GBC_200_' + str(11) + '.joblib')
    model = load('model_GBC_' + str(11) + '.joblib')
    ans = model.predict(test_x)

    pred['LABEL_Sepsis'] = ans
    
    #task 3
    
    for i in range(11, 15):
        print(i)
        train_y = train_labels.iloc[:, [i + 1]].to_numpy()
        model = make_pipeline(StandardScaler(), svm.SVR(cache_size=5000, C=1, kernel='rbf', coef0=1))
        model.fit(train_x, np.ravel(train_y))
        dump(model, 'model_c_1_' + str(i+1) + '.joblib')

        # model = load('zero_models/model_zero_' + str(i+1) + '.joblib')
        ans = model.predict(test_x)
        pred[columns[i+1]] = ans
    
    print(pred)

    count = []
    for i in range (12):
        c = 0
        for j in range(pred.shape[0]):
            if pred.iloc[j, i] == 0: 
                c += 1
        count.append(c)
    
    print(count)
    print(pred.shape[0])
    pred.to_csv('submission.zip', index=False, float_format='%.3f', compression="zip")
      
    
    """
    
    #Testing
    VITALS = ['LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']
    TESTS = ['LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
            'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2',
            'LABEL_Bilirubin_direct', 'LABEL_EtCO2']
    
    df_true = train_labels

    print(get_score(df_true, pred))
 
    #task1 = np.mean([metrics.roc_auc_score(df_true[entry], pred[entry]) for entry in TESTS])
    #task2 = metrics.roc_auc_score(df_true['LABEL_Sepsis'], pred['LABEL_Sepsis'])
    #task3 = np.mean([0.5 + 0.5 * np.maximum(0, metrics.r2_score(df_true[entry], pred[entry])) for entry in VITALS])
 
    #print(task3)
    #score = np.mean([task1, task2, task3])
    #print(task1, task2, task3)
    #print(score)

    #print(task2) 

    """
    

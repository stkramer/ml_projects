import torch
from torch import nn
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader, Dataset, random_split


class CustomDataset(Dataset):
    def __init__(self, X, y):
        self.data = X
        self.labels = y

    def __len__(self):
        return min(len(self.labels), len(self.data))
    
    def __getitem__(self, idx):
        return torch.tensor(self.data[idx]), torch.tensor(self.labels[idx])

class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(36, 64),
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(32, 1),
            nn.Sigmoid()
        )
    
    def train_(self, dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)
        model.train()
        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)
            # Compute prediction error
            pred = model(X)
            loss = loss_fn(pred, y)

            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch % 100 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")
    
    def test_(self, dataloader, model, loss_fn):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        model.eval()
        test_loss, correct = 0, 0
        with torch.no_grad():
            for X, y in dataloader:
                X, y = X.to(device), y.to(device)
                pred = model(X)

                test_loss += loss_fn(pred, y).item()
                correct += (pred == y).type(torch.float).sum().item()
        test_loss /= num_batches
        correct /= size
        print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits.squeeze()



if __name__ == '__main__':
    device = "cuda" if torch.cuda.is_available() else "cpu"

    columns = ['pid', 'LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
           'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2', 'LABEL_Bilirubin_direct', 'LABEL_EtCO2', 'LABEL_Sepsis', 'LABEL_RRate', 'LABEL_ABPm', 'LABEL_SpO2', 'LABEL_Heartrate']
    
    train_labels = pd.read_csv("train_labels.csv")
    train_data = pd.read_csv("train_data_zero.csv")
    test_data = pd.read_csv("test_data_zero.csv")

    pred = pd.DataFrame(columns=columns)
    pred['pid'] = test_data.iloc[:,0]
    



    #task1
    train_X = train_data.iloc[:, 1:].to_numpy().astype('f')
    test_X = test_data.iloc[:, 1:].to_numpy().astype('f')

    epochs = 5
    for i in range(10):
        train_y = train_labels.iloc[:, i+1].to_numpy().astype('f')


        batch_size = 64
        train_dataset = CustomDataset(train_X, train_y)
        train_size = int( 0.8 * len(train_data))
        test_size = len(train_data) - train_size

        print(len(train_dataset))
        train_dataset, test_dataset = random_split(train_dataset, [train_size, test_size], generator=torch.Generator().manual_seed(420))
        print(len(train_dataset))
        print(len(test_dataset))

        train_dataloader = DataLoader(train_dataset, batch_size=batch_size)
        test_dataloader = DataLoader(test_dataset, batch_size=batch_size)
            
        model = NeuralNetwork().to(device)
        loss_fn = nn.BCELoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)

        for t in range(epochs):
            print(f"Epoch {t+1}\n-------------------------------")
            model.train_(train_dataloader, model, loss_fn, optimizer)
            model.test_(test_dataloader, model, loss_fn)
        print("Done!")
        torch.save(model.state_dict(), f"model_1_{i}.pth")

    #for (X, y) in enumerate(test_dataloader):
    #    pred[columns[1]] = model(X)
    

    #pred.to_csv("prediction.csv")

    # #task 2
    # train_y = train_labels.iloc[:, [11]].to_numpy()

    # train_dataset = CustomDataset(train_X, train_y)
    # test_dataset = CustomDataset(test_X, test_y)

    # train_dataloader = DataLoader(train_dataset, batch_size=batch_size)
    # test_dataloader = DataLoader(test_dataset, batch_size=batch_size)
    
    # model = NeuralNetwork().to(device)
    # loss_fn = nn.CrossEntropyLoss()
    # optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)

    # epochs = 5
    # for t in range(epochs):
    #     print(f"Epoch {t+1}\n-------------------------------")
    #     model.train_(train_dataloader, model, loss_fn, optimizer)
    #     model.test_(test_dataloader, model, loss_fn)
    # print("Done!")

    # torch.save(model.state_dict(), "model.pth")
    # print("Saved PyTorch Model State to model.pth")

    # pred['LABEL_Sepsis'] = model.predict(test_X)

    # #task 3
    
    # for i in range(11, 15):
    #     print(i)
    #     train_y = train_labels.iloc[:, [i + 1]].to_numpy()

    #     train_dataset = CustomDataset(train_X, train_y)
    #     test_dataset = CustomDataset(test_X, test_y)

    #     train_dataloader = DataLoader(train_dataset, batch_size=batch_size)
    #     test_dataloader = DataLoader(test_dataset, batch_size=batch_size)
        
    #     model = NeuralNetwork().to(device)
    #     loss_fn = nn.CrossEntropyLoss()
    #     optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)

    #     epochs = 5
    #     for t in range(epochs):
    #         print(f"Epoch {t+1}\n-------------------------------")
    #         model.train_(train_dataloader, model, loss_fn, optimizer)
    #         model.test_(test_dataloader, model, loss_fn)
    #     print("Done!")

    #     torch.save(model.state_dict(), "model.pth")
    #     print("Saved PyTorch Model State to model.pth")

    #     pred[columns[i+1]] = model.predict(test_X)
    
    # print(pred)

    # count = []
    # for i in range (12):
    #     c = 0
    #     for j in range(pred.shape[0]):
    #         if pred.iloc[j, i] == 0: 
    #             c += 1
    #     count.append(c)
    
    # print(count)
    # print(pred.shape[0])
    # pred.to_csv('nn_test.csv', index=False, float_format='%.3f')
    

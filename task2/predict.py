from cgi import test
import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer


def data_modification(array, filename):
    
    pid = array.iloc[:, 0]
    age = array.iloc[:, 2]
    data = array.iloc[:, 3:]

    train_data = pd.DataFrame(columns=list(range(410)))

    length = len(data)
    for i in range(int(length / 12)):
        rows = (data.iloc[(12 * i): (12 * i) + 12, :]).to_numpy()
        temp = np.append([], rows)
        temp = np.insert(temp, 0, pid[i*12])
        temp = np.insert(temp, 1, age[i*12])
        train_data = np.vstack((train_data, temp))


    train_data_pd = pd.DataFrame(train_data)

    imp_mean = SimpleImputer(missing_values=np.nan,strategy='mean')
    imp_mean.fit(train_data_pd)
    train_data_pd = pd.DataFrame(imp_mean.transform(train_data_pd))

    print(train_data_pd)

    train_data_pd.to_csv(filename, index=False)


if __name__ == '__main__':
    
    train_features = pd.read_csv("train_features.csv")
    train_labels = pd.read_csv("train_labels.csv")
    test_features = pd.read_csv("test_features.csv")

    data_modification(train_features, filename='train_many_cols.csv')
    data_modification(test_features, filename='test_many_cols.csv')
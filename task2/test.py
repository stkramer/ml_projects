from cmath import nan
import numpy as np
import pandas as pd
from sklearn import metrics
from imblearn.under_sampling import RandomUnderSampler as UnderSampler
from imblearn.over_sampling import RandomOverSampler as OverSampler


columns = ['pid', 'LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
           'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2', 'LABEL_Bilirubin_direct', 'LABEL_EtCO2', 'LABEL_Sepsis']

TESTS = ['LABEL_BaseExcess', 'LABEL_Fibrinogen', 'LABEL_AST', 'LABEL_Alkalinephos', 'LABEL_Bilirubin_total',
        'LABEL_Lactate', 'LABEL_TroponinI', 'LABEL_SaO2',
        'LABEL_Bilirubin_direct', 'LABEL_EtCO2']



if __name__ == '__main__':
    train_labels = pd.read_csv("train_labels.csv")

    count = []

    for i in range (12):
        c = 0
        for j in range(train_labels.shape[0]):

            if train_labels.iloc[j, i] == 0: 
                c += 1

        count.append(c)
    
    print(count)

    """
    print(train_labels.shape[0])
    train_features = pd.read_csv("train_features.csv")
    
    print(train_features.Bilirubin_direct.mean())
    print(train_features.isnull().sum())
    print(train_features.shape[0])

    t = {'a': [2, nan], 'b' : [4,8], 'c' : [nan, nan]}
    arr = pd.DataFrame(data=t)

    print(arr.mean(axis=0))

    print(train_features.mean())

    print(train_features.shape[1])

    """
    train_data = pd.read_csv('train_data_zero.csv')
    train_y = train_labels.iloc[:, 1]

    length = train_data.shape[0]
    zeros = train_y[train_y == 0].count()
    print(length)
    print(zeros)
    print(zeros/length)


    train_data = pd.read_csv('train_data_zero.csv').to_numpy()
    train_y = train_labels.iloc[:, 1].to_numpy()

    print("start")

    us = UnderSampler()
    train_data_sampled, train_y_sampled = us.fit_resample(train_data, np.ravel(train_y))

    

    #train_data, train_y = undersampling(train_data, np.ravel(train_y))
    
    train_data_sampled = pd.DataFrame(train_data_sampled)
    train_y_sampled = pd.DataFrame(train_y_sampled.reshape(-1,1))
    train_y_sampled.to_csv('ttt.csv', index=False)
    #print(train_data_sampled)
    #print(train_y_sampled)
    
    length = train_data_sampled.shape[0]
    zeros = train_y_sampled[train_y_sampled == 0].count()
    print(length)
    print(zeros)
    print(zeros/length)

    




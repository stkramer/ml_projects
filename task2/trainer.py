import threading
import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer
from score_submission import get_score
from sklearn import svm
from sklearn import metrics
from joblib import dump, load
from sklearn.linear_model import SGDClassifier
from sklearn.kernel_ridge import KernelRidge



class Trainer:

    def train(self, train_x, train_y, model, path, number):
        model.fit(train_x, train_y)
        dump(model, path + str(number) + '.joblib')
        print(number)

        
        
        
    
    def __init__(self, train_x, train_y, model, path, number):
        t = threading.Thread(target=self.train(train_x, train_y, model, path, number))
        t.start()
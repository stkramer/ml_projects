from pickle import TRUE
from redone import Trainer
import numpy as np
from tqdm import tqdm

#This takes long
pte=[20, 50, 150]
ptlr=[1e-4, 1e-5]
ptwd=[1e-4, 1e-5]
indim=[128]

#This is fast
tre=[200, 400, 1000]
trlr=[1e-4, 1e-5]
trwd=[1e-4, 1e-5]

fte=[500, 1000, 2500]
ftlr=[1e-4, 1e-5]
ftwd=[1e-4, 1e-5]

uf=[-3]

min_loss=1000
k=10
PRINT=True

#To try different parameters, do this and set PRINT to false
#To do just one iteration, either enter just one value per list or
#change the defaults in redone.py and just do tr=Trainer() tr.train()...

for i, (PT_E,PT_LR,PT_WD,TR_E,TR_LR,TR_WD,FT_E,FT_LR,FT_WD,INTER_DIM,UNFREEZE) in \
    tqdm(list(enumerate([(PT_E,PT_LR,PT_WD,TR_E,TR_LR,TR_WD,FT_E,FT_LR,FT_WD,INTER_DIM,UNFREEZE) \
    for PT_E in pte for PT_LR in ptlr for PT_WD in ptwd for INTER_DIM in indim \
    for TR_E in tre for TR_LR in trlr for TR_WD in trwd for FT_E in fte for FT_LR in ftlr for FT_WD in ftwd for UNFREEZE in uf]))):
    args=(PT_E,PT_LR,PT_WD,TR_E,TR_LR,TR_WD,FT_E,FT_LR,FT_WD,INTER_DIM,UNFREEZE)
    tr = Trainer(PT_E=PT_E,PT_LR=PT_LR,PT_WD=PT_WD,TR_E=TR_E,TR_LR=TR_LR,TR_WD=TR_WD,FT_E=FT_E,FT_LR=FT_LR, \
                    FT_WD=FT_WD,INTER_DIM=INTER_DIM,UNFREEZE=UNFREEZE,PRINT=PRINT,k=k)
    loss = tr.train()
    print("\================== Round",i,"=======================")
    print("Arguments: ", args)
    print("Loss:", loss)
    if loss<min_loss:
        min_loss=loss
        min_args=args
        min_tr = tr
print("\n\nMinimum loss achieved:")
print(min_args)
print(min_loss)

min_tr.predict()
min_tr.plot()
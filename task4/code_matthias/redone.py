from math import nan
import copy
import os.path as osp
from pkgutil import get_loader
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch.utils.data import TensorDataset, DataLoader
import numpy as np
from tqdm import tqdm

#Variable short forms used:
#   Training consists of the following steps:
#      - Pretraining on 50'000 images(PT)
#      - Training an additional model on 100 images with pretraining output = input (TR)
#      - Finetuning the model after unfreezing some layers (FT)
#      - Possibly more finetuning with more layers unfrozen (FFT) Probalby not helpful and left out for now
#   Training has the following variables:
#       - Epochs (E)
#       - Learning Rate (LR)
#       - Weight Decay (WD)


class Trainer:
    def __init__(self,PT_E=100,PT_LR=5e-5,PT_WD=1e-4,TR_E=600,TR_LR=5e-5,TR_WD=1e-5,FT_E=1000,FT_LR=1e-5,FT_WD=1e-5,
                        BATCH_SIZE=5000, INTER_DIM=256, UNFREEZE=0,PRINT=True,k=10):
        #Assigning the arguments to class variables
        self.PT_E   = PT_E
        self.PT_LR  = PT_LR
        self.PT_WD  = PT_WD
        self.TR_E   = TR_E
        self.TR_LR  = TR_LR
        self.TR_WD  = TR_WD
        self.FT_E   = FT_E
        self.FT_LR  = FT_LR
        self.FT_WD  = FT_WD
        self.UNFREEZE = UNFREEZE
        self.BS     = BATCH_SIZE
        self.PRINT  = PRINT
        self.k      = k
        #Creating a unique name for the pretrained model
        self.model_name = ""
        for arg in [PT_E, PT_LR, PT_WD, INTER_DIM]:
            self.model_name += str(arg)

        #Some more class variables
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.res = []
        self.model  = None
        self.data   = None
        self.labels = None
        self.optim = None
        self.train_loader = None
        self.valid_loader = None
        self.loss_fn = self.RMSELoss
        self.eval_fn = torch.nn.MSELoss()
        #There is a model for pretraining and one for finetuning that gets appended to the pretrained one
        self.PT_model = torch.nn.Sequential(
                            torch.nn.Linear(1000, 2048),
                            torch.nn.Sigmoid(),
                            torch.nn.Dropout(0.25),
                            torch.nn.Linear(2048, 1024),
                            torch.nn.Sigmoid(),
                            torch.nn.Dropout(0.25),
                            torch.nn.Linear(1024,INTER_DIM),
                            torch.nn.BatchNorm1d(INTER_DIM),
                            torch.nn.Sigmoid(),
                            torch.nn.Dropout(0.25),
                            torch.nn.Linear(INTER_DIM,1)
                        )
        # self.PT_model = torch.nn.Sequential(
        #                     torch.nn.Linear(1000, 2048),
        #                     torch.nn.Dropout(0.5),
        #                     torch.nn.ReLU(),
        #                     torch.nn.Linear(2048,INTER_DIM),
        #                     torch.nn.Dropout(0.5),
        #                     torch.nn.ReLU(),
        #                     torch.nn.Linear(INTER_DIM,1)
        #                 )
        self.FT_model_appendix = torch.nn.Sequential(
                            torch.nn.Linear(INTER_DIM,256),
                            torch.nn.Sigmoid(),
                            torch.nn.Dropout(0.25),
                            torch.nn.Linear(256,64),
                            torch.nn.Sigmoid(),
                            torch.nn.Dropout(0.25),
                            torch.nn.Linear(64,1)
                            )
        #These are for plotting the loss
        self.train_acc_pt = []
        self.valid_acc_pt = []
        self.train_acc_tr = [[] for i in range(10)]
        self.valid_acc_tr = [[] for i in range(10)]
        self.train_acc_ft = [[] for i in range(10)]
        self.valid_acc_ft = [[] for i in range(10)]

    def RMSELoss(self, x,y):
        return torch.sqrt(torch.mean((x-y)**2))

    #Assigns the class data loader with according train/valid-split
    def get_loaders(self, train_percentage=0.9):
        num_data = len(self.data)
        perm = np.random.permutation(num_data)
        idx_split_train = perm[:int(train_percentage*num_data)]
        idx_split_valid = perm[int(train_percentage*num_data):]

        train_data = self.data[idx_split_train]
        valid_data = self.data[idx_split_valid]
        train_labels = self.labels[idx_split_train]
        valid_labels = self.labels[idx_split_valid]

        train_dataset = TensorDataset(train_data, train_labels)
        valid_dataset = TensorDataset(valid_data, valid_labels)

        self.train_loader = DataLoader(train_dataset, shuffle=True, batch_size=self.BS)
        if train_percentage < 1.0:
            self.valid_loader = DataLoader(valid_dataset, shuffle=True, batch_size=self.BS)
        else:
            self.valid_loader = None

    #Does one epoch of training
    def train_step(self, data_loader):
        self.model.train() # enable dropout, batch norms

        loss_total = 0
        itms_total = 0
        for data, labels in data_loader:
            data = data.to(self.device)
            labels = labels.to(self.device)
            
            self.optim.zero_grad()

            pred = self.model(data)
            loss = self.loss_fn(pred, labels)
            batch_size = labels.size(0)
            loss_total += loss.item() * batch_size
            itms_total += batch_size

            loss.backward()
            self.optim.step()

        return loss_total / itms_total
    
    #Evaluates the model
    def eval_model(self, data_loader):
        self.model.eval() # disable any dropout, batch norms
        
        itms_total = 0
        loss_total = 0

        for data, labels in data_loader:
            data = data.to(self.device)
            labels = labels.to(self.device)

            with torch.no_grad(): # disable gradient computation
                pred = self.model(data)
                loss = self.eval_fn(pred, labels)
                batch_size = labels.size(0)
                loss_total += loss.item() * batch_size
                itms_total += batch_size
        return (loss_total / itms_total) ** 0.5

    #The actual training
    def train(self):
        #=============================PT Phase==================================

        if self.PRINT: print("Starting Pretraining Phase")

        #TODO: Possibly standardize data, problem: std=0 for some -> Exclude from data?
        #Assign Training phase specific variables
        self.model = self.PT_model.to(self.device)
        self.optim = torch.optim.Adam(self.model.parameters(), lr=self.PT_LR, weight_decay=self.PT_WD)
        df_data = pd.read_csv(osp.join("dataset", "pretrain_features.csv.zip")).iloc[:,2:]
        self.data = torch.tensor(df_data.values).float()
        df_labels = pd.read_csv(osp.join("dataset", "pretrain_labels.csv.zip")).iloc[:,1:2]
        self.labels = torch.tensor(df_labels.values).float()

        #Check if this model has already been trained
        if osp.exists(osp.join('presaves',self.model_name+".pt")):
            if self.PRINT: print("Loading pretrained model...")
            self.model.load_state_dict(torch.load(osp.join('presaves',self.model_name+".pt")))
            if osp.exists(osp.join('presaves',self.model_name+".npz")):
                plotstats = np.load(osp.join('presaves',self.model_name+".npz"))
                self.train_acc_pt=plotstats["train_acc_pt"]
                self.valid_acc_pt=plotstats["valid_acc_pt"]
            else:
                if self.PRINT: print("No plot data available for pretraining")
        else:
            self.get_loaders(0.8)
            if self.PRINT: print("Pretraining model...")
            
            count=0
            #Train for the given amount of epochs, using early stopping when count reaches threshold
            for epoch in tqdm(range(self.PT_E)) if self.PRINT else range(self.PT_E):
                if epoch == 0:
                    self.train_acc_pt.append(self.eval_model(self.train_loader))
                    min_v_loss = self.eval_model(self.valid_loader)
                    self.valid_acc_pt.append(min_v_loss)
                    

                # Training
                self.train_step(self.train_loader)

                # Evaluation
                self.train_acc_pt.append(self.eval_model(self.train_loader))
                curr_v_loss = self.eval_model(self.valid_loader)
                self.valid_acc_pt.append(curr_v_loss)

                #Early stopping
                if curr_v_loss > min_v_loss:
                    count += 1
                    if count > 100:
                        if self.PRINT: print("\nStopped Pretraining after",epoch-100,"epochs with early stopping")
                        break
                else:
                    count = 0
                    min_v_loss = curr_v_loss
                    torch.save(self.model.state_dict(), osp.join('presaves',self.model_name+".pt"))
            np.savez_compressed(osp.join('presaves',self.model_name+".npz"), train_acc_pt=self.train_acc_pt,valid_acc_pt=self.valid_acc_pt)

        #========================== Training + Finetuning Phase =============================================
        #We use k-fold validation with 10% as validation data. 
        # ATTENTION:
        # If k=1, no validation is done and therefore no early stopping
        # So make sure to set your epochs right or you will have overfitting
        
        #Preparing things that stay constant for k-fold validation
        perm = np.random.permutation(100)
        df_data = pd.read_csv(osp.join("dataset", "train_features.csv.zip")).iloc[:,2:]
        self.data = torch.tensor(df_data.values).float()[perm]
        df_labels = pd.read_csv(osp.join("dataset", "train_labels.csv.zip")).iloc[:,1:2]
        self.labels = torch.tensor(df_labels.values).float()[perm]

        PT_layers = list(self.model.children())
        tmp_model = torch.nn.Sequential(
                        *(PT_layers[:-1]),
                        *self.FT_model_appendix
        )
        #We freeze all the layers from the pretrained model
        for i in range(len(PT_layers)-1):
            if i <len(PT_layers)-1:
                for param in list(tmp_model.children())[i].parameters():
                    param.requires_grad = False
            else:
                for param in list(tmp_model.children())[i].parameters():
                    param.requires_grad = True

        if self.PRINT: print("Starting K-Fold Training and Finetuning Phase")
        
        errsum = 0
        for fold in tqdm(range(self.k)) if self.PRINT else range(self.k):
            #Prepare varibles vor one fold iteration
            self.model = copy.deepcopy(tmp_model).to(self.device)
            train_data = torch.cat((self.data[:int(10*fold)],self.data[int(10*(fold+1)):]))
            valid_data = self.data[10*fold:10*(fold+1)]
            train_labels = torch.cat((self.labels[:int(10*fold)],self.labels[int(10*(fold+1)):]))
            valid_labels = self.labels[10*fold:10*(fold+1)]
            if self.k==1:
                train_data=self.data
                train_labels=self.labels

            train_dataset = TensorDataset(train_data, train_labels)
            valid_dataset = TensorDataset(valid_data, valid_labels)

            self.train_loader = DataLoader(train_dataset, shuffle=True, batch_size=self.BS)
            self.valid_loader = DataLoader(valid_dataset, shuffle=True, batch_size=self.BS)

            self.optim = torch.optim.Adam(self.model.parameters(), lr=self.TR_LR, weight_decay=self.TR_WD)

            #=======Training Phase========
            count=0
            for epoch in range(self.TR_E):
                if epoch == 0:
                    self.train_acc_tr[fold].append(self.eval_model(self.train_loader))
                    min_v_loss = self.eval_model(self.valid_loader)
                    self.valid_acc_tr[fold].append(min_v_loss)
                    

                # Training
                self.train_step(self.train_loader)

                # Evaluation
                self.train_acc_tr[fold].append(self.eval_model(self.train_loader))
                curr_v_loss = self.eval_model(self.valid_loader)
                self.valid_acc_tr[fold].append(curr_v_loss)

                #Early stopping
                if curr_v_loss > min_v_loss:
                    count += 1
                    # if count > 300 and self.k>1:
                        # if self.PRINT: print("\nStopped Training after",epoch-50,"epochs with early stopping")
                        # break
                else:
                    count = 0
                    min_v_loss = curr_v_loss
            if self.PRINT: print("\nTR Best after",self.TR_E-count,"epochs")
            #================ Finetuning Phase ================

            #Here we unfreeze some layers from the pretrained model.
            for i in [0,3,6]:
                if i>self.UNFREEZE:
                    for param in list(self.model.children())[i].parameters():
                        param.requires_grad = True

            self.optim = torch.optim.Adam(self.model.parameters(), lr=self.FT_LR, weight_decay=self.FT_WD)

            count=0
            for epoch in range(self.FT_E):
                if epoch == 0:
                    self.train_acc_ft[fold].append(self.eval_model(self.train_loader))
                    min_v_loss = self.eval_model(self.valid_loader)
                    self.valid_acc_ft[fold].append(min_v_loss)
                    

                # Training
                self.train_step(self.train_loader)

                # Evaluation
                self.train_acc_ft[fold].append(self.eval_model(self.train_loader))
                curr_v_loss = self.eval_model(self.valid_loader)
                self.valid_acc_ft[fold].append(curr_v_loss)

                #Early stopping
                if curr_v_loss > min_v_loss:
                    count += 1
                    # if count > 300 and self.k>1:
                    #     if self.PRINT: print("Stopped Finetuning after",epoch-50,"epochs with early stopping")
                    #     break
                else:
                    count = 0
                    min_v_loss = curr_v_loss
            if self.PRINT: print("FT Best after",self.FT_E-count,"epochs")
            if self.PRINT: print("Loss in iteration",fold,":",curr_v_loss)
            errsum+= curr_v_loss
        return errsum/self.k

    #Predicts the testset
    def predict(self):
        test_feats = pd.read_csv(osp.join("dataset", "test_features.csv.zip"))
        test_feats = torch.tensor(test_feats.iloc[:,2:].values).float()
        test_loader = DataLoader(test_feats, shuffle=False, batch_size=self.BS)

        self.model.eval() # disable any dropout, batch norms
            
        res = np.empty((0,1),int)
        if self.PRINT: print("\n>>> Predicting test dataset...")
        for data in tqdm(test_loader) if self.PRINT else test_loader:
            data = data.to(self.device)
            with torch.no_grad(): # disable gradient computation

                preds = self.model(data)
                res = np.append(res, np.array(preds.cpu()))

        with open('result.csv', 'w') as f:
            f.write("Id,y\n")
            for i, x in enumerate(res):
                f.write(str(50100+i)+","+str(x)+'\n')
        self.res=res
    #Plots some stuff
    def plot(self):
        if len(self.train_acc_pt)>0:
            plt.title("Pretraining Loss")
            plt.xlabel("Epoch")
            plt.ylabel("MSE")
            plt.semilogy(self.train_acc_pt, label="training data")
            plt.semilogy(self.valid_acc_pt, label="validation data")
            plt.legend(loc="upper left")
            plt.show()

        
        fig, axes = plt.subplots(2,2)

        ax = axes[0,0]
        ax.set_title("Training TrainLoss")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("MSE")
        extt = [[xi[m] if m<len(xi) else nan for m in range(self.TR_E)] for xi in self.train_acc_tr ]
        dt =np.transpose(np.array([np.array(xi) for xi in extt]))
        ax.semilogy(dt, label="training data")

        ax = axes[0,1]
        ax.set_title("Training ValLoss")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("MSE")
        extv = [[xi[m] if m<len(xi) else nan for m in range(self.TR_E)] for xi in self.valid_acc_tr ]
        dv =np.transpose(np.array([np.array(xi) for xi in extv]))
        ax.semilogy(dv, label="validation data")

        ax = axes[1,0]
        ax.set_title("Finetuning TrainLoss")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("MSE")
        extt = [[xi[m] if m<len(xi) else nan for m in range(self.FT_E)] for xi in self.train_acc_ft ]
        dt =np.transpose(np.array([np.array(xi) for xi in extt]))
        ax.semilogy(dt, label="training data")

        ax = axes[1,1]
        ax.set_title("Finetuning ValLoss")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("MSE")
        extv = [[xi[m] if m<len(xi) else nan for m in range(self.FT_E)] for xi in self.valid_acc_ft ]
        dv =np.transpose(np.array([np.array(xi) for xi in extv]))
        ax.semilogy(dv, label="validation data")
        if len(self.res)>0:
            fig, axes = plt.subplots(2,2,sharex=False)
            ax = axes[0,0]
            ax.set_title("Histogramm 100 labels")
            lbls = np.array(pd.read_csv(osp.join("dataset", "train_labels.csv.zip")).iloc[:,1:2])
            ax.hist(lbls)
            ax = axes[1,0]
            ax.set_title("Histogramm PT labels")
            lbls = np.array(pd.read_csv(osp.join("dataset", "pretrain_labels.csv.zip")).iloc[:,1:2])
            ax.hist(lbls)
            ax = axes[0,1]
            ax.set_title("Histogramm preds")
            ax.hist(self.res)
            ax = axes[1,1]
            ax.set_title("Histogramm preds")
            ax.hist(self.res)
            plt.show()
import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import csv


class AEDataset(Dataset):
    def __init__(self, csv_file):
        df = pd.read_csv(csv_file).iloc[:, 2:]
        # TODO: how to convert str (list?) in a good way?
        # df['smiles'] = df['smiles'].apply(lambda x: list(map(int, x)))
        # print(df['smiles'][0])
        self.data = df
    
    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return torch.tensor(self.data.iloc[idx], dtype=torch.float)


class TrainDataset(Dataset):
    def __init__(self, features_csv, labels_csv, model):
        X_df = pd.read_csv(features_csv).iloc[:, 2:]
        # TODO: how to convert str (list?) in a good way?
        # X_df['smiles'][0] = X_df['smiles'][0].apply(lambda x: list(map(int, x)))
        # print(X_df['smiles'][0])
        self.features = X_df
        self.labels = pd.read_csv(labels_csv).iloc[:, 1]
        self.model = model
    
    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        input = torch.tensor(self.features.iloc[idx], dtype=torch.float)
        additional_features = self.model.encode(input)

        return (torch.cat((input, additional_features)), torch.tensor(self.labels.iloc[idx], dtype=torch.float))


class HomoLumoDataset(Dataset):
    def __init__(self, features_csv, labels_csv, ae_model, lumo_model):
        X_df = pd.read_csv(features_csv).iloc[:, 2:]
        # TODO: how to convert str (list?) in a good way?
        # X_df['smiles'][0] = X_df['smiles'][0].apply(lambda x: list(map(int, x)))
        # print(X_df['smiles'][0])
        self.features = X_df
        self.labels = pd.read_csv(labels_csv).iloc[:, 1]
        self.ae_model = ae_model
        self.lumo_model = lumo_model

    def __len__(self):
        return len(self.labels)
    
    def __getitem__(self, idx):
        input = torch.tensor(self.features.iloc[idx], dtype=torch.float)
        ae_features = self.ae_model.encode(input)
        lumo_input = torch.cat((input, ae_features))
        lumo_features = self.lumo_model.encode(lumo_input)

        return (lumo_features, torch.tensor(self.labels.iloc[idx], dtype=torch.float))


class AE(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(1000, 256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 16)
        )

        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(16, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, 1000),
            torch.nn.Sigmoid()
        )
    
    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
    
    def encode(self, x):
        return self.encoder(x)


class LumoNN(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.nn = torch.nn.Sequential(
            torch.nn.Linear(1016, 256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 32),
            torch.nn.ReLU(),
            torch.nn.Linear(32, 16),
            torch.nn.ReLU()
        )

        self.lumo = torch.nn.Sequential(
            torch.nn.Linear(16, 1)
        )
    
    def forward(self, x):
        features = self.nn(x)
        return self.lumo(features)
    
    def encode(self, x):
        return self.nn(x)


class HomoLumoNN(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.nn = torch.nn.Sequential(
            torch.nn.Linear(16, 16),
            torch.nn.ReLU(),
            torch.nn.Linear(16, 8),
            torch.nn.ReLU(),
            torch.nn.Linear(8, 1)
        )
    
    def forward(self, x):
        return self.nn(x)

def ae_train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, x in enumerate(dataloader):
        pred = model(x)
        loss = loss_fn(x, pred)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(x) * 3
            print(f'loss: {loss:>7f}  [{current:>5d}/{size:>5d}]')

def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (x, y) in enumerate(dataloader):
        pred = model(x).type(torch.float)
        loss = loss_fn(y, pred)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(x) * 3
            print(f'loss: {loss:>7f}  [{current:>5d}/{size:>5d}]')


def ae_test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for x in dataloader:
            pred = model(x)
            loss = loss_fn(x, pred)
            test_loss += loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f'Test Error \n Accuracy: {(100 * correct):>1f}%, Avg loss: {test_loss:>8f}')

def test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for (x, y) in dataloader:
            pred = model(x)
            loss = loss_fn(y, pred)
            test_loss += loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f'Test Error \n Accuracy: {(100 * correct):>1f}%, Avg loss: {test_loss:>8f}')


if __name__ == '__main__':
    ae_model = AE()
    ae_loss_fn = torch.nn.MSELoss()
    ae_optimizer = torch.optim.Adam(ae_model.parameters(), lr=1e-2, weight_decay=1e-8)

    ae_data = AEDataset('data/pretrain_features.csv')

    ae_train_set_size = int(len(ae_data) * 0.9)
    ae_valid_set_size = len(ae_data) - ae_train_set_size
    ae_pretrain_set, ae_pretrain_valid_set = torch.utils.data.random_split(ae_data, [ae_train_set_size, ae_valid_set_size])

    dataloader_train = DataLoader(ae_pretrain_set, batch_size=128, shuffle=False)
    dataloader_test = DataLoader(ae_pretrain_valid_set, batch_size=128, shuffle=False)

    # Train AutoEncoder
    epochs = 5
    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        ae_train_loop(dataloader_train, ae_model, ae_loss_fn, ae_optimizer)
        ae_test_loop(dataloader_test, ae_model, ae_loss_fn)

    print("Done!")
    torch.save(ae_model.state_dict(), 'models/ae_model.pth')

    # Train LumoNN with pretrain dataset
    lumo_model = LumoNN()
    loss_fn = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(ae_model.parameters(), lr=1e-2, weight_decay=1e-8)

    pretrain_data = TrainDataset('data/pretrain_features.csv', 'data/pretrain_labels.csv', ae_model)

    train_set_size = int(len(pretrain_data) * 0.9)
    valid_set_size = len(pretrain_data) - train_set_size
    pretrain_set, pretrain_valid_set = torch.utils.data.random_split(pretrain_data, [train_set_size, valid_set_size])

    dataloader_train = DataLoader(pretrain_set, batch_size=128, shuffle=False)
    dataloader_test = DataLoader(pretrain_valid_set, batch_size=128, shuffle=False)

    epochs = 10
    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train_loop(dataloader_train, lumo_model, loss_fn, optimizer)
        test_loop(dataloader_test, lumo_model, loss_fn)

    print("Done!")
    torch.save(lumo_model.state_dict(), 'models/lumo_model.pth')
    
    # Train HomoLumoNN with train dataset
    homolumo_model = HomoLumoNN()
    loss_fn = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(ae_model.parameters(), lr=1e-2, weight_decay=1e-8)

    train_data = HomoLumoDataset('data/train_features.csv', 'data/train_labels.csv', ae_model, lumo_model)

    train_set_size = int(len(train_data) * 0.9)
    valid_set_size = len(train_data) - train_set_size
    train_set, train_valid_set = torch.utils.data.random_split(train_data, [train_set_size, valid_set_size])

    dataloader_train = DataLoader(train_set, batch_size=16, shuffle=False)
    dataloader_test = DataLoader(train_valid_set, batch_size=16, shuffle=False)

    epochs = 10
    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train_loop(dataloader_train, homolumo_model, loss_fn, optimizer)
        test_loop(dataloader_test, homolumo_model, loss_fn)

    print("Done!")
    torch.save(homolumo_model.state_dict(), 'models/homolumo_model.pth')

    # Get predictions on test set
    df = pd.read_csv('data/test_features.csv')
    ids = df.iloc[:, 0]
    train_data = df.iloc[:, 2:]

    predictions = []
    pred_file = open('predictions.csv', 'w', newline='')
    writer = csv.writer(pred_file, delimiter=',')
    fields = ['Id','y']
    writer.writerow(fields)
    for i in range(len(train_data)):
        row = torch.tensor(train_data.iloc[i, :], dtype=torch.float)
        ae_features = ae_model.encode(row)
        lumo_features = lumo_model.encode(torch.cat((row, ae_features)))
        pred = homolumo_model(lumo_features).detach().numpy()
        # print(type(pred))
        tmp = pred[0]

        predictions.append(tmp)
        pred_data = [(50100+i), tmp]
        writer.writerow(pred_data)

    

    """
    #print(predictions)
    print(type(predictions))
    # predictions = np.reshape(predictions, (10000,2))
    
    print(ids.shape)
    print(predictions.shape)
    res = np.vstack((ids, predictions))
    pd.DataFrame(res).to_csv('predictions.csv', index=False)
    """
from math import comb
from matplotlib.pyplot import cla
import torch
import numpy as np
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import csv


class LUMO_model(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.linear = torch.nn.Sequential(
            torch.nn.Linear(1000, 2000),
            torch.nn.Sigmoid(),
            torch.nn.Dropout(0.25),
            torch.nn.Linear(2000, 1000),
            torch.nn.Sigmoid(),
            torch.nn.Dropout(0.25),
            torch.nn.Linear(1000, 256),
            torch.nn.BatchNorm1d(256),
            torch.nn.Sigmoid(),
            torch.nn.Dropout(0.25),
        )
        self.output = torch.nn.Linear(256,1)

        self.layers = len(list(self.children()))

    def forward(self, x):
        return self.output(self.linear(x))


class HUMO_LUMO_model(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.nn = torch.nn.Sequential(
            torch.nn.Linear(256, 512),
            torch.nn.Sigmoid(),
            torch.nn.Dropout(0.25),
            torch.nn.Linear(512, 128),
            torch.nn.Sigmoid(),
            torch.nn.Dropout(0.25),
            torch.nn.Linear(128, 1),
            
       )
    
    def forward(self, x):
        return self.nn(x)


class TrainDataset(Dataset):
    def __init__(self, features_csv, labels_csv):
        X_df = pd.read_csv(features_csv).iloc[:, 2:]
        # TODO: how to convert str (list?) in a good way?
        # X_df['smiles'][0] = X_df['smiles'][0].apply(lambda x: list(map(int, x)))
        # print(X_df['smiles'][0])
        self.features = X_df
        self.labels = pd.read_csv(labels_csv).iloc[:, 1]
        
    
    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        data = torch.tensor(self.features.iloc[idx], dtype=torch.float).to('cuda')
        label = torch.tensor(self.labels.iloc[idx], dtype=torch.float).to('cuda')
        return (data, label)


def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (x, y) in enumerate(dataloader):
        # print(x.size())
        pred = model(x).type(torch.float)
        pred = torch.squeeze(pred, 1)
        loss = torch.sqrt(loss_fn(pred, y))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(x) * 3
            print(f'loss: {loss:>7f}  [{current:>5d}/{size:>5d}]')

def test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for (x, y) in dataloader:
            pred = model(x).type(torch.float)
            pred = torch.squeeze(pred, 1)
            # print("Prediction_size: " + str(pred.size()))
            # print("target_size: " + str(y.size()))
            loss = torch.sqrt(loss_fn(pred, y))
            test_loss += loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f'Test Error \n Accuracy: {(100 * correct):>1f}%, Avg loss: {test_loss:>8f}')



if __name__=='__main__':
    # device = 'cuda' if torch.cuda.is_available() else 'cpu'
    torch.manual_seed(0)

    pretrain_data = TrainDataset(features_csv='data/pretrain_features.csv', labels_csv='data/pretrain_labels.csv')
    length_pretrain_set = (int) (0.9 * len(pretrain_data))
    length_validation_set = len(pretrain_data) - length_pretrain_set
    pretrain_set, validation_set = torch.utils.data.random_split(pretrain_data, [length_pretrain_set, length_validation_set])

    dataloader_pretrain = DataLoader(pretrain_set, batch_size=512, shuffle=False)
    dataloader_pretrain_test = DataLoader(validation_set, batch_size=512, shuffle=False)

    lumo_model = LUMO_model().to('cuda')
    loss_fn = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(lumo_model.parameters(), lr=1e-4, weight_decay=1e-5)

    epochs = 150

    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train_loop(dataloader_pretrain, lumo_model, loss_fn, optimizer)
        test_loop(dataloader_pretrain_test, lumo_model, loss_fn)

    print("Done")
    # print(list(lumo_model.modules()))
    humo_lumo_Model = HUMO_LUMO_model().to('cuda')
    layers_lumo = list(lumo_model.children())
    print(layers_lumo[:-1])
    combined_Model = torch.nn.Sequential(
                        *layers_lumo[:-1],
                        *list(humo_lumo_Model.children())).to('cuda')
    
    for i in range(lumo_model.layers - 1):
        for weights in list(combined_Model.children())[i].parameters():
            weights.requires_grad = False
    
    
    loss_fn = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(combined_Model.parameters(), lr=1e-4, weight_decay=1e-5)

    train_data = TrainDataset(features_csv='data/train_features.csv', labels_csv='data/train_labels.csv')
    length_train_set = (int) (len(train_data) * 0.9)
    length_validation_set = len(train_data) - length_train_set
    train_set, validation_set = torch.utils.data.random_split(train_data, [length_train_set, length_validation_set])

    dataloader_train = DataLoader(train_data, batch_size=100, shuffle=False)
    # dataloader_test = DataLoader(validation_set, batch_size=512, shuffle=False)

    print("Model:")
    print(combined_Model)

    epochs = 400

    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train_loop(dataloader_train, combined_Model, loss_fn, optimizer)
        # test_loop(dataloader_pretrain_test, combined_Model, loss_fn)

    print("Combined Model done")

    # Fine Tuning --> unfreeze linear layers of lumo-model
    print("layers: " + str(lumo_model.layers - 1))
    for i in range(lumo_model.layers - 1):
        for weights in list(combined_Model.children())[3* i].parameters():
            weights.requires_grad = True

    optimizer = torch.optim.Adam(combined_Model.parameters(), lr=1e-4, weight_decay=1e-5)
    loss_fn = torch.nn.MSELoss()

    epochs = 2500

    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train_loop(dataloader_train, combined_Model, loss_fn, optimizer)

    print("Fine Tuning done")

    # Prediction
    combined_Model.eval()

    df = pd.read_csv('data/test_features.csv')
    ids = df.iloc[:, 0]
    train_data = df.iloc[:, 2:]

    predictions = []
    pred_file = open('predictions.csv', 'w', newline='')
    writer = csv.writer(pred_file, delimiter=',')
    fields = ['Id','y']
    writer.writerow(fields)
    for i in range(len(train_data)):
        with torch.no_grad():
            row = torch.tensor(train_data.iloc[i, :], dtype=torch.float).to('cuda')
            # print(row.size())
            pred = combined_Model(row.unsqueeze(0)).cpu()
            # print(type(pred))
            tmp = pred[0]

            predictions.append(tmp)
            pred_data = [(50100+i), tmp]
            writer.writerow(pred_data)


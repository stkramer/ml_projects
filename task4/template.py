import torch
import torch.nn as nn
from torchvision import models
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
from PIL import Image
import pandas as pd

IMAGES = 'food/{}.jpg'

class Data(Dataset):

    def __init__(self, path) -> None:
        super().__init__()
        self.features = pd.read_csv('extracted_features_b3_norm.csv')
        self.train_triplets = pd.read_csv(path, sep=' ', dtype=int, header=None)

            
    def __getitem__(self, index):
        device = 'cuda'
        triple = self.train_triplets.iloc[index, :]
        img_base = torch.tensor(self.features.iloc[triple[0], :], dtype=torch.float).to(device)
        img_pos = torch.tensor(self.features.iloc[triple[1], :], dtype=torch.float).to(device)
        img_neg = torch.tensor(self.features.iloc[triple[2], :], dtype=torch.float).to(device)
        
        return img_base, img_pos, img_neg, torch.tensor([0.0]).to(device)


    def __len__(self):
        return len(self.train_triplets)


class Model(nn.Module):

    def __init__(self) -> None:
        super().__init__()

        self.stack = nn.Sequential(
            nn.Linear(1536, 1536, bias=True),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(1536, 1024, bias=True),
            nn.ReLU(),
            nn.Dropout(0.5), 
            nn.Linear(1024, 256, bias=True),
            nn.ReLU(),
        )

    def forward(self, img):
        return self.stack(img)


def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (img_base, img_1, img_2, y) in enumerate(dataloader):
        pred_b = model(img_base)
        pred_1 = model(img_1)
        pred_2 = model(img_2)
        loss = loss_fn(pred_b, pred_1, pred_2)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(img_base) * 3
            print(f'loss: {loss:>7f}  [{current:>5d}/{size:>5d}]')


def test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for img_base, img_1, img_2, y in dataloader:
            pred_b = model(img_base)
            pred_1 = model(img_1)
            pred_2 = model(img_2)
            loss = loss_fn(pred_b, pred_1, pred_2)
            test_loss += loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f'Test Error \n Accuracy: {(100 * correct):>1f}%, Avg loss: {test_loss:>8f}')


def get_image_features():
    device = 'cuda'

    model = models.efficientnet_b3(pretrained=True).to(device)
    # model = torch.load('entire_model_img_b3.pth', map_location=device)
    new_model = nn.Sequential(*list(model.children())[:-1]).eval().to(device)
    # print(new_model)
    # model = torch.load('model_weights_101.pth', map_location='cuda')
    # model.classifier = nn.Sequential(*list(model.classifier.children())[:-1])
    # print(model.classifier)
    # model = model.eval()
    features = []

    with torch.no_grad():
        for i in range(10000):
            img = Image.open(IMAGES.format(str(i).zfill(5)))
            img = transform_image(img)

            tmp = new_model(img)
            tmp = tmp.view(-1)
            if (i % 1000 == 0):
                print(i)
            features.append(tmp.squeeze().to('cpu').numpy())

    pd.DataFrame(features).to_csv("extracted_features_b3_norm.csv", index=False)
    print("done")
          

def transform_image(img):
    device = 'cuda'

    # transform = transforms.Compose([transforms.Resize(255),
    #                                   transforms.CenterCrop(224),
    #                                   transforms.ToTensor(),
    #                                   transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
    transform = transforms.Compose([transforms.Resize((350,450)), transforms.ToTensor(), transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225],
    )])
    return transform(img).unsqueeze(0).to(device)


if __name__ == '__main__':
    torch.manual_seed(0)
    device ='cuda'

    # pretraining
    get_image_features() 

    # train the model
    model = Model().to(device)
    # model.load_state_dict(torch.load('model_weights_triplet_b3_norm.pth'))
    data_train = Data('train_triplets.txt')
    
    train_set_size = int(len(data_train) * 0.9)
    valid_set_size = len(data_train) - train_set_size
    train_set, valid_set = torch.utils.data.random_split(data_train, [train_set_size, valid_set_size])

    loss_fn = nn.TripletMarginLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0005)

    dataloader_train = DataLoader(train_set, batch_size=128, shuffle=False)
    dataloader_test = DataLoader(valid_set, batch_size=128, shuffle=False)
    
    epochs = 15
    try:
        for t in range(epochs):
            print(f"Epoch {t + 1}\n-------------------------------")
            train_loop(dataloader_train, model, loss_fn, optimizer)
            test_loop(dataloader_test, model, loss_fn)
        print("Done!")
        torch.save(model.state_dict(), 'model_weights_triplet_b3_norm.pth')
    except KeyboardInterrupt:
        print('Abort...')
        safe = input('Safe model [y]es/[n]o: ')
        if safe == 'y' or safe == 'Y':
            torch.save(model.state_dict(), 'model_weights_triplet_b3_norm.pth')
        else: 
            print('Not saving...')
    

    # predict
    test_triplets = pd.read_csv('test_triplets.txt', sep=' ', dtype=int, header=None)
    features = pd.read_csv('extracted_features_b3_norm.csv')

    model = Model().to(device)
    model.load_state_dict(torch.load('model_weights_triplet_b3_norm.pth'))
    model.eval()

    print(test_triplets.shape)
    predictions = []

    triplet_loss = torch.nn.TripletMarginLoss(margin=1.0, p=2)
    for index in range(test_triplets.shape[0]):
        row = test_triplets.iloc[index, :]
        img_base = torch.tensor(features.iloc[row[0], :], dtype=torch.float).to(device)
        img_1 = torch.tensor(features.iloc[row[1], :], dtype=torch.float).to(device)
        img_2 = torch.tensor(features.iloc[row[2], :], dtype=torch.float).to(device)

        pred_b = model(img_base.unsqueeze(0))
        pred_1 = model(img_1.unsqueeze(0))
        pred_2 = model(img_2.unsqueeze(0))
        
        d_b1 = triplet_loss(pred_b, pred_1, pred_2).item()
        d_b2 = triplet_loss(pred_b, pred_2, pred_1).item()

        
        if (d_b1 <= d_b2):
            predictions.append(1)
        else:
            predictions.append(0)
        
    with open('predictions.txt', 'w') as file:
        for p in predictions:
            file.write(str(p) + '\n')
